<?php namespace Rasyid\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRasyidContact extends Migration
{
    public function up()
    {
        Schema::create('rasyid_contact_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191)->nullable();
            $table->text('message');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('rasyid_contact_');
    }
}
