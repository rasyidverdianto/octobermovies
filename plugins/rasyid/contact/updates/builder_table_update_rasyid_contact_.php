<?php namespace Rasyid\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRasyidContact extends Migration
{
    public function up()
    {
        Schema::table('rasyid_contact_', function($table)
        {
            $table->string('email')->nullable();
            $table->string('name', 191)->default('null')->change();
        });
    }
    
    public function down()
    {
        Schema::table('rasyid_contact_', function($table)
        {
            $table->dropColumn('email');
            $table->string('name', 191)->default('NULL')->change();
        });
    }
}
