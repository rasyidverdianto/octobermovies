<?php namespace Rasyid\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteRasyidContact extends Migration
{
    public function up()
    {
        Schema::dropIfExists('rasyid_contact_');
    }
    
    public function down()
    {
        Schema::create('rasyid_contact_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191)->nullable()->default('\'null\'');
            $table->text('message');
            $table->string('email', 255)->nullable()->default('NULL');
        });
    }
}
