<?php namespace Rasyid\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRasyidContact2 extends Migration
{
    public function up()
    {
        Schema::table('rasyid_contact_', function($table)
        {
            $table->text('content')->nullable()->default('null');
            $table->string('name', 255)->default('null')->change();
            $table->string('email', 255)->default('null')->change();
            $table->dropColumn('message');
        });
    }
    
    public function down()
    {
        Schema::table('rasyid_contact_', function($table)
        {
            $table->dropColumn('content');
            $table->string('name', 255)->default('NULL')->change();
            $table->string('email', 255)->default('NULL')->change();
            $table->text('message')->nullable()->default('NULL');
        });
    }
}
