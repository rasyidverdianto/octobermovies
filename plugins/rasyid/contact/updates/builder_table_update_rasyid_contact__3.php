<?php namespace Rasyid\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRasyidContact3 extends Migration
{
    public function up()
    {
        Schema::table('rasyid_contact_', function($table)
        {
            $table->string('name', 255)->default('null')->change();
            $table->string('email', 255)->default('null')->change();
            $table->text('content')->default('null')->change();
        });
    }
    
    public function down()
    {
        Schema::table('rasyid_contact_', function($table)
        {
            $table->string('name', 255)->default('\'null\'')->change();
            $table->string('email', 255)->default('\'null\'')->change();
            $table->text('content')->default('\'null\'')->change();
        });
    }
}
