<?php namespace Rasyid\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRasyidContact2 extends Migration
{
    public function up()
    {
        Schema::create('rasyid_contact_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->text('message')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('rasyid_contact_');
    }
}
