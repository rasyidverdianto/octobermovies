<?php namespace Rasyid\Movies\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use Redirect;
use Rasyid\Movies\Models\Actor;
use Flash;



Class ActorForm extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Actor Form',
            'description' => 'Enter Actors'
        ];
    }
    public function onSave()
    {
        $actor = New Actor();
        $actor->name = Input::get('name');
        $actor->lastname = Input::get('lastname');
        $actor->actorimage = Input::file('actorimage');
        $actor->save();

        Flash::success('Actor added!');
        return Redirect::back();
    }  
}