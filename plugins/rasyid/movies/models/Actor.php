<?php namespace Rasyid\Movies\Models;

use Model;

/**
 * Model
 */
class Actor extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'rasyid_movies_actors';


    public $belongsToMany =[
        'movies' =>[
            'Rasyid\Movies\Models\Movie',
            'table' => 'rasyid_movies_actors_movies',
            'order' => 'name'
        ]
    ];

    public $attachOne =[
        'actorimage' => 'System\Models\File'
    ];

    public function getFullNameAttribute(){
        return $this->name . " " . $this->lastname;
    }
}