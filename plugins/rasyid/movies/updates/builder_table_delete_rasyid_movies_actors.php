<?php namespace Rasyid\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteRasyidMoviesActors extends Migration
{
    public function up()
    {
        Schema::dropIfExists('rasyid_movies_actors');
    }
    
    public function down()
    {
        Schema::create('rasyid_movies_actors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
        });
    }
}