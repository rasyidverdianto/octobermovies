<?php namespace Rasyid\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRasyidMoviesGenres extends Migration
{
    public function up()
    {
        Schema::create('rasyid_movies_genres', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('genre_title');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('rasyid_movies_genres');
    }
}