<?php namespace Rasyid\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRasyidMovies2 extends Migration
{
    public function up()
    {
        Schema::table('rasyid_movies_', function($table)
        {
            $table->text('actors')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('rasyid_movies_', function($table)
        {
            $table->dropColumn('actors');
        });
    }
}