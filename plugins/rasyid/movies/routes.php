<?php
    use Rasyid\Movies\Models\Movie;

    Route::get('hello', function(){

        return Movie::orderBy('year', 'asc')->paginate(20);
    });

?>
